# Easy PlatformIO ESP32 OTA Starter Template

https://tinyurl.com/yxb2f767

Allows for Static IP and custom Host Name.
Includes Putty for debugging on Windoze via Telnet.

# To use:
1) Rename `credentials-sample.h` to `credentials.h`
2) Change values in `credentials.h`
3) Upload template sketch via USB
4) Uncomment `upload_port` and `upload_protocol` lines in platformio.ini file
5) Change `upload_port` to the static IP you defined in `credentials.h`
6) Now you can upload the sketch via WiFi
7) Don't let the main loop of your sketch take longer than 5 seconds or you will have to use USB to upload sketch again.
8) To debug, open PuTTy (included) and TelNet to your Static IP port 23
