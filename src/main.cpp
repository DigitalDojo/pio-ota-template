#include <OTA.h>
#include <TelnetStream.h>

void setup() {

  Serial.begin(9600);
  Serial.println("Booting");

  // myDNAME should be defined in credentials.h
  setupOTA(myDNAME);
}
 

void loop() {
  
  #ifndef ESP32_RTOS
  ArduinoOTA.handle();
  #endif
  
  // example of how to write stuff to telnet for debug
  // Use PuTTy to Debug on static IP port 23
  TelnetStream.print("Inside the ");
  TelnetStream.println("loop");

  // don't let this loop take longer than 5 seconds or OTA won't work. You'll need to upload via USB if that happens.
  // best is to not use any delays and use millis() to keep track of polling
  delay(2000);
}
