// First change this file name to just credentials.h

#define mySSID "YourSSIDnetworkNAME"
#define myPWD "YourSSIDpwd"
#define myDNAME "ESP32-DEVICENAME"

// Request a static IP. Usually you just need to change the last integer in this array,
// eg:  { 192, 168, 1, 220 }
#define myStaticIP { 192, 168, 1, 221 }

// Gateway is the IP of your modem.
#define myGateway { 192, 168, 1, 1 }
#define mySubnet { 255, 255, 0, 0 }

// These are Google's DNS servers.
#define myPrimaryDNS { 8, 8, 8, 8 }
#define mySecondaryDNS { 8, 8, 4, 4 }

